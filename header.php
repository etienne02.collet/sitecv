<?php 
session_start();
function inverseLang($init) {
    $change = array("fr"=> "en", "en"=> "fr");
    return $change[$init];
}
?>

<script>
    // gestion de la langue
    function inverseLang(init) {
        let change = {"fr": "en", "en": "fr"};
        return change[init];
    }

    function changeLang() {
        let params = {
        "method": "POST",
        "headers": {
            "Content-Type": "application/json; charset=utf-8"
        },
        "body": JSON.stringify({"lang": inverseLang("<?php echo($_SESSION['lang']); ?>")})
        }

        fetch("lang.php", params)
        .then(res => res.text())
        .then(txt => {if(txt === "fr" || txt === "en"){location.reload();}})
        .catch(err => console.error(err));
    }

</script>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="header_style.css">
</head>

<header>
    <div class="container-header">
        <div class="main-header">
            <div class="info-header">
                <div class="title-header">
                    ETIENNE COLLET
                </div>
                <!--partie langue-->
                <div class="container-lang">
                    <input id="check02" type="checkbox" name="menu" />
                    <label for="check02">
                        <img src="./.media/<?php echo($_SESSION['lang']);?>.jpeg" class="img-main-l"/>
                    </label>

                    <div class="container-choice">
                        <button type="button" onclick="changeLang()" class="choice">
                            <img src="./.media/<?php echo(inverseLang($_SESSION['lang']));?>.jpeg" class="img-choice"/>
                        </button>
                    </div>
                </div>
            </div>

            <div class="menu-header">
                <input id="check01" type="checkbox" name="menu" />
                <!--bouton pour responsive-->
                <label for="check01">
                    <div class="button-menu-header">
                        <img src="./.media/menu.png" class="logo-header">
                    </div>
                </label>

                <div class="test">
                    <ul class="list-header">
                        <div id="divone" class="button">
                            <a id="one" class="" href="#home" onclick="myfun('one')"><?php echo($text['generic']['home'])?></a>
                        </div>
                        <div id="divtwo" class="button">
                            <a id="two" class="" href="#pp" onclick="myfun('two')"><?php echo($text['generic']['pp'])?></a>
                        </div>
                        <div id="divthree" class="button">
                            <a id="three" class="" href="#tp" onclick="myfun('three')"><?php echo($text['generic']['tp'])?></a>
                        </div>
                        <!--<div id="divfour" class="button">
                            <a id="four" class="" href="#up" onclick="myfun('four')"><?php echo($text['generic']['up'])?></a>
                        </div>-->
                        <div id="divfive" class="button">
                            <a id="five" class="" href="#other" onclick="myfun('five')"><?php echo($text['generic']['other'])?></a>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<script>
    // permet de gerer le menu 
    var last_id = "";
    switch (window.location.hash) {
        case "#up":
            last_id = "four-this";
            break;
        case "#pp":
            last_id = "two-this";
            break;
        case "#tp":
            last_id = "three-this";
            break;
        case "#other":
            last_id = "five-this";
            break;
        case "#home":
        default:
            last_id = "one-this";
    }
    let last_elem = document.getElementById((last_id.split("-")[0]));
    let last_div_elem = document.getElementById("div" + (last_id.split("-")[0]));
    last_elem.setAttribute("id", last_id);
    last_div_elem.setAttribute("id", "div" + last_id);

    function myfun(new_id) {
        if (last_id == (new_id + "-this")) {
            return;
        }
        let last_elem = document.getElementById(last_id);
        let last_div_elem = document.getElementById("div" + last_id);
        let new_elem = document.getElementById(new_id);
        let new_div_elem = document.getElementById("div" + new_id);

        last_elem.setAttribute("id", last_id.split("-")[0]);
        last_div_elem.setAttribute("id", "div" + (last_id.split("-")[0]));

        new_elem.setAttribute("id", new_id + "-this");
        new_div_elem.setAttribute("id", "div" + new_id + "-this");

        last_id = new_id + "-this";

        let params = {
        "method": "POST",
        "headers": {
            "Content-Type": "application/json; charset=utf-8"
        },
        "body": JSON.stringify({"link": new_elem.href, "lang": "<?php echo($_SESSION['lang']);?>"})
        }

        fetch("save.php", params)
        .then(res => res.text())
        .catch(err => console.error(err));

        document.getElementById("check01").checked = false;
        return false;
    }
</script>

</html>