<?php
session_start();

if(isset($_POST)){
    $data = file_get_contents("php://input");
    $array_data = json_decode($data, true);
    if($array_data !== null){
        $_SESSION['lang'] = $array_data['lang'];
        echo $_SESSION['lang'];
        return;
    }
}

if(!$_SESSION["lang"]){
    $_SESSION["lang"]="fr";
}
return false;