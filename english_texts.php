<?php
$text = [
    'home' => [
        'title' => 'PhD computer science student',
        'title_hook' => "I want to do a CIFRE PhD in computer science in September 2024, I'm looking for a company that can offer me a subject oriented towards complexity or image processing problems",
        'info_name' => "Etienne Collet<br/>21 years",
        'info_mail' => "etienne02.collet@gmail.com",
        'info_phone' => "+33 06 18 62 41 33",
        'info_address' => "Noisy-le-Grand 93160, France",
        'info_linkedin' => "Linkedin",
        'info_github' => "Github",

        'info_skills_title' => "SKILLS",
        'info_skills_sub_title_one' => "WEB :",
        'info_skills_sub_info_one' => "HTML, CSS, PHP, JS, Laravel, SLIM, REACT",
        'info_skills_sub_title_two' => "Software :",
        'info_skills_sub_info_two' => "C, C++, Python, JAVA",
        'info_skills_sub_title_three' => "Database :",
        'info_skills_sub_info_three' => "SQL",
        'info_skills_sub_title_four' => "Other :",
        'info_skills_sub_info_four' => "YACC, BISON, LATEX",

        'info_skills_office' => "Mastery of office automation tools",
        'info_skills_driver_license' => "Holder of a B driving licence (vehicle)",

        'info_langage_title' => "LANGUAGE",
        'info_langage_one' => "French ( Native language )",
        'info_langage_two' => "Technical English",
        'info_langage_two_download' => "Download<br/>The Certification",

        'info_hobbies_title' => "HOBBIES",
        'info_hobbies_one' => "Chess",
        'info_hobbies_two' => "Climbing",
        'info_hobbies_three' => "Fishing",
        'info_hobbies_four' => "LEGO",
        'info_hobbies_five' => "Mountain biking",
        'info_hobbies_six' => "",

        'info_other_xp_title' => "Other Expériences",
        'info_other_xp_one_title' => "February - MaY 2022: Mc Donald’s",
        'info_other_xp_one' => "Polyvalent team member",
        'info_other_xp_two_title' => "2018 - 2020 : Grimpe à Noisy",
        'info_other_xp_two' => "Climbing teacher<br/>Children's groups from 12 to 16 years",
        
        'formation_title' => "Trainings :",
        'formation_one' => "2024 – 2027 Computer science thesis",
        'formation_two' => "2022 – 2024 Master's 2 in Computer Science<br/>
            Institut d’électronique et d’informatique Gaspard-Monge (IGM)<br/>
            Image specialisation in the second year",
        'formation_bourse' => "2023 Awarded the Labex Bézout merit scholarship",
        'formation_three' => "2019 – 2022 Mathematics - Computer Science degree<br/>
            Gustave Eiffel University ( Mention Bien )",

        'project_title' => "Projects :",
        'project_one_title' => "January 2023 – October 2023 : IGM",
        'project_one_subject' => "Research internship, subject : ",
        'project_one_question' => "Distinct shortest walk enumeration",
        'project_one_context_title' => "Context :",
        'project_one_context' => "Discovery of research, supervised by 4 researchers",
        'project_one_realization_title' => "Production :",
        'project_one_realization_one' => "Write theoretical proofs of algorithms",
        'project_one_realization_two' => "Implementation of an innovative algorithm for data graphs",
        'project_one_realization_three' => "Discussions and exchanges on the theses",
        'project_one_realization_four' => "",
        'project_one_technical_title' => "Technical environment :",
        'project_one_technical' => "Use of Python and NetworkX for proofs of concept",

        'project_two_title' => "October 2021 – June 2022 : IGM",
        'project_two_subject' => "Tutored project, subject : ",
        'project_two_question' => "Complex phase portraits",
        'project_two_context_title' => "Context :",
        'project_two_context' => "Carrying out optimisation on a Python application with a high degree of autonomy",
        'project_two_realization_title' => "Production :",
        'project_two_realization_one' => "Setting up a profiler to study an unconventional algorithm",
        'project_two_realization_two' => "Created a C library to get round the slowness of Python",
        'project_two_realization_three' => "Performance testing with large data sets",
        'project_two_realization_four' => "",
        'project_two_technical_title' => "Technical environment :",
        'project_two_technical' => "Based on a python application <br/> abstraction of display functions to time calculation functions",
        
        'xp_title' => "Expérences :",
        'xp_one_title' => "August 2022 - September 2024 : Globalis Média Système",
        'xp_one_subject' => "Function : ",
        'xp_one_function' => "development engineer (work-study)",
        'xp_one_context_title' => "Context :",
        'xp_one_context' => "Web development company founded in 1997 based on very strong technical expertise in PHP",
        'xp_one_realization_title' => "Production :",
        'xp_one_realization_one' => "Correction, development and creation of web applications",
        'xp_one_realization_two' => "Algorithm creation with a particular focus on optimisation and technical rigour",
        'xp_one_realization_three' => "Examples of websites: e-certif, Rallye clés en main or Oqali",
        'xp_one_realization_four' => "",
        'xp_one_technical_title' => "Technical environment :",
        'xp_one_technical' => "Very high technical requirements on Slim, Laravel, React and SQL with 6 or 7 simultaneous projects",

        'xp_two_title' => "September 2021 et 2023 : IGM",
        'xp_two_subject' => "Tutoring in Computer Science and Mathematics",
        'xp_two_function' => "Providing revision courses for first-year students",
    ],
    'pp' => [

    ],
    'tp' => [

    ],
    'up' => [

    ],
    'other' => [
    
    ],
    'generic' => [
        'download' => "Download",
        'french' => "French",
        'english' => "English",
        'home' => "Home",
        'pp' => "Personnal Project",
        'up' => "University Project",
        'tp' => "Tutored Project",
        'other' => "Other",
    ],
];