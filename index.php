<?php 
session_start();

require_once("mysql.php");
require_once("lang.php");

if(!$_SESSION["newsession"]){
    $_SESSION["newsession"]=random_int(0, 999999);
}

if($_SESSION["lang"] === "fr"){
    require_once("french_texts.php");
}else{
    require_once("english_texts.php");
}
?>

<!DOCTYPE html>
<html lang="[fr, en]">

<head>
    <meta charset="utf-8">
    <title>CV Etienne Collet</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="./home/style.css">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=yes" />
</head>

<script>
    if (!window.location.hash) {
        location.hash = '#home';
    }
</script>

<body>
    <?php require_once("header.php"); ?>
    <div class="container">
        <div id="home" class="part">
            <?php require_once("./home/index.php");  ?>
        </div>
        <div id="pp" class="part">
            <?php require_once("./personnal-project/index.php");  ?>
        </div>
        <div id="tp" class="part">
            <?php require_once("./tutored-project/index.php");  ?>
        </div>
        <div id="up" class="part">
            <?php require_once("./university-project/index.php");  ?>
        </div>
        <div id="other" class="part">
            <?php require_once("./other/index.php");  ?>
        </div>
    </div>
    <?php require_once("footer.php");  ?>
</body>

</html>