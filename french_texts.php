<?php
$text = [
    'home' => [
        'title' => 'Doctorant en Informatique',
        'title_hook' => "Voulant faire un doctorat CIFRE en informatique en septembre 2024,
            je recherche une entreprise pouvant me proposer un sujet orientée vers 
            des problèmes de complexité ou de traitement d’images",
        'info_name' => "Etienne Collet<br/>21 ans",
        'info_mail' => "etienne02.collet@gmail.com",
        'info_phone' => "06 18 62 41 33",
        'info_address' => "France, Noisy-le-Grand 93160",
        'info_linkedin' => "Linkedin",
        'info_github' => "Github",

        'info_skills_title' => "COMPETENCE",
        'info_skills_sub_title_one' => "WEB :",
        'info_skills_sub_info_one' => "HTML, CSS, PHP, JS, Laravel, SLIM, REACT",
        'info_skills_sub_title_two' => "Logiciel :",
        'info_skills_sub_info_two' => "C, C++, Python, JAVA",
        'info_skills_sub_title_three' => "Base de données :",
        'info_skills_sub_info_three' => "SQL",
        'info_skills_sub_title_four' => "Autre :",
        'info_skills_sub_info_four' => "YACC, BISON, LATEX",

        'info_skills_office' => "Maitrise des outils de bureautiques",
        'info_skills_driver_license' => "Détenteur du permis B (Véhiculé)",

        'info_langage_title' => "LANGUE",
        'info_langage_one' => "Français ( Langue maternelle )",
        'info_langage_two' => "Anglais technique",
        'info_langage_two_download' => "Télécharger<br/>La Certification",

        'info_hobbies_title' => "LOISIRS",
        'info_hobbies_one' => "Echecs",
        'info_hobbies_two' => "Escalade",
        'info_hobbies_three' => "Pêche",
        'info_hobbies_four' => "LEGO",
        'info_hobbies_five' => "VTT",
        'info_hobbies_six' => "",

        'info_other_xp_title' => "Autre Expériences",
        'info_other_xp_one_title' => "Février - Mai 2022: Mc Donald’s",
        'info_other_xp_one' => "Equipier polyvalent",
        'info_other_xp_two_title' => "2018 - 2020 : Grimpe à Noisy",
        'info_other_xp_two' => "Professeur d’escalade<br/>Groupe d’enfant de 12 à 16 ans",
        
        'formation_title' => "Formations :",
        'formation_one' => "2024 – 2027 Doctorat en Informatique",
        'formation_two' => "2022 – 2024 Master 2 Informatique<br/>
            Institut d’électronique et d’informatique Gaspard-Monge (IGM)<br/>
            Spécialisation en image en seconde année",
        'formation_bourse' => "2023 Obtention de la bourse au mérite du Labex Bézout",
        'formation_three' => "2019 – 2022 Licence Mathématiques – Informatique <br/>
            Université Gustave Eiffel ( Mention Bien )",

        'project_title' => "Projets :",
        'project_one_title' => "Janvier 2023 – Octobre 2023 : IGM",
        'project_one_subject' => "Stage de recherche, sujet : ",
        'project_one_question' => "Énumération des plus cours chemin dans un Graphe de données",
        'project_one_context_title' => "Contexte :",
        'project_one_context' => "Découverte de la recherche encadrer par 4 chercheurs",
        'project_one_realization_title' => "Réalisation :",
        'project_one_realization_one' => "Écrire des preuves théorique d’algorithme",
        'project_one_realization_two' => "Mise en place d’un algorithme novateur sur les graphes de données",
        'project_one_realization_three' => "Discutions et échange sur les thèses",
        'project_one_realization_four' => "",
        'project_one_technical_title' => "Environnement technique :",
        'project_one_technical' => "Utilisation de Python et NetworkX pour les preuves de concept",

        'project_two_title' => "Octobre 2021 – Juin 2022 : IGM",
        'project_two_subject' => "Projet tutoré, sujet : ",
        'project_two_question' => "Complex phase portraits",
        'project_two_context_title' => "Contexte :",
        'project_two_context' => "Réalisation d’optimisation sur une application python en grande autonomie",
        'project_two_realization_title' => "Réalisation :",
        'project_two_realization_one' => "Mise en place de profiler pour étudier un algorithme inconue",
        'project_two_realization_two' => "Créé une librairie en C pour contourner la lenteur du python",
        'project_two_realization_three' => "Réalisation de test de performance avec de grosse donnée",
        'project_two_realization_four' => "",
        'project_two_technical_title' => "Environnement technique :",
        'project_two_technical' => "Sur la base d’une application python <br/> abstraction des fonction d’affichage 
            pour chronométré les fonctions de calcul.",
        
        'xp_title' => "Expérences :",
        'xp_one_title' => "Aout 2022 - Septembre 2024 : Globalis Média Système",
        'xp_one_subject' => "Fonction : ",
        'xp_one_function' => "ingénieur développement (Alternant)",
        'xp_one_context_title' => "Contexte :",
        'xp_one_context' => "Entreprise de développement web fondée en 1997 se basant sur 
            Une très forte expertise technique sur PHP",
        'xp_one_realization_title' => "Réalisation :",
        'xp_one_realization_one' => "Correction, évolution et création d’application web.",
        'xp_one_realization_two' => "Création d'algorithme avec une attention perticuliere sur l'optimisation et la rigueur technique",
        'xp_one_realization_three' => "Exemple de sites : e-certif, Rallye clés en main ou encore Oqali",
        'xp_one_realization_four' => "",
        'xp_one_technical_title' => "Environnement technique :",
        'xp_one_technical' => "Très haute exigence technique sur Slim, Laravel, React et SQL 
            avec 6 ou 7 projets en simultané",

        'xp_two_title' => "Septembre 2021 et 2023 : IGM",
        'xp_two_subject' => "Tutorat d’Informatique et de Mathématiques",
        'xp_two_function' => "Dispenser des cours de révision pour les première années",
    ],
    'pp' => [
        'title_one' => "Mise en place de mon server",
        'discribe_one' => "Ce projet, vous avez l'avez devant les yeux. Lorsque j'ai voulu mettre en place 
            mon propre site internet, j'ai décidé de ne pas utiliser d'hébergeur. Après une longue phase 
            de rechercher, j'ai récupéré un ancien ordinateur DELL que j'ai refait fonctionner. Une fois 
            fonctionnel, j'ai retiré Windows pour installer une base LAMP. Après cela, je suis passé au 
            réseau. Une partie compliquée qui m'as permis d'appliquer mes cours de L3. Une fois ouvert 
            vers le monde, j'ai commencé à implémenter mon site internet avec du PHP, HTML, CSS et JS. 
            J'ai porté une attention particulière à ce qu'il soit responsive. Cependant, il y a encore 
            quelques erreurs, ainsi celui-ci, est en constante évolution.constante évolution.",

        'title_two' => "Snake en 3D",
        'discribe_two' => "Partant de l'intention de découvrir les mathématiques qui se cachent derrière 
            un affichage en trois dimensions, ce projet implémentes le célèbre jeu SNAKE avec une vue en 
            première personne. Ainsi, pendant les deux semaines de vacances de Noël lors de ma L2 j'en ai 
            profité pour coder ce petit jeu. J'ai commencé par implémenter le jeu en deux dimensions. 
            Lors de cette première phase, j'ai fait attention à ce que le jeu soit scalable pour que plus 
            tard cela se transforme en mini map. Par la suite, j'ai commencé l'implémentation en 3D en 
            définissant un point de fuite. Ensuite, l'affichage de la grille au sol a été l'une des étapes 
            les plus compliquées avec de nombreuses erreurs de calcul. J'ai aussi implémenté l'affichage 
            des murs et de la pomme. Pour finir le projet, j'ai affiché le corps du serpent ce qui a aussi 
            été une étape périlleuse, car initialement, je voulais que ce soit tubulaire. Cependant, 
            je n'ai pas réussi à avoir un rendu correct et je me suis rabattu vers un corps rectangulaire. 
            Par la suite, je n'ai pas eu le temps d'implémenter les transitions, car les cours ont repris.",
    ],
    'tp' => [
        'top_bar_one' => "Base de donées",
        'top_bar_two' => "Phase Portrait",
        'title_one' => "Énumération des plus cours chemin dans un Graphe de données",
        'discribe_one' => "Cet exposé en français m’a permis de découvrir le monde de la recherche et qui à
            susciter mon envie de faire un doctorat. Il traite du problème suivant : comment peut-
            on énumérer de manière efficace les plus courts chemins distincts dans un graphe de
            données ? Nous traitons donc ce sujet d’un point de vue majoritairement théorique. Nous
            commençons tout d’abord avec des rappels comme le produit cartésien, les langages,
            les automates et les Graphes de données. On exprime ensuite les requêtes que l’on va
            proposer à ce modèle de donnée ainsi que leur fonctionnement. Pour finir cette partie,
            nous exposons la complexité qui va nous permettre d’évaluer notre algorithme. Pour
            enchaîner avec le corps du projet, la présentation de notre algorithme avec le pseudo-
            code, la preuve de concept et la preuve de correction qui lui sont associées. Enfin, cet
            exposé se termine par les différentes perspectives de continuation du projet. Un projet
            que je suis très fière de vous présenter et dont j’espère que vous prendrez du plaisir à
            découvrir.",
        'title_two' => "Phase Portrait",
        'discribe_two' => "Ce projet tuteuré qui c'est dérouler lors de ma dernière année de licence, 
            porte sur la génération de Portrait de Phase. Lors de ce projet, l'objectif était : 
            l'optimisation des étapes de calcul et de génération des Portraits de Phase. 
            Cet exposé commence par rappeler qu'est ce qu'un Portrait de phase et qu'elles sont les techniques 
            utilisées dans notre cas pour les générer. Suite à cela, nous introduisons les différentes 
            technologies que nous avons utilisées pour optimiser le code. Enfin, je finis par exposer 
            les changements apportés dans le code ainsi que les résultats que j'obtiens. Initialement, 
            le code était relativement naïf, mais nous avons quand même obtenu une accélération 15 fois 
            meilleure à la fin du projet. Dans ce document, nous finissons avec une liste de conclusions et 
            d'amélioration possible, mais comme j'ai continué le projet en parallèle, ces dernières sont 
            peut-être déjà traitées. Le nouveau rapport avec les différentes nouvelles améliorations est 
            en cours de rédaction."
    ],
    'up' => [

    ],
    'other' => [
        'title_one' => "Documents importants",
        'paper_one' => "Buletin de note L1",
        'paper_two' => "Buletin de note L2",
        'paper_three' => "Buletin de note de L3",
        'paper_four' => "Buletin de note de M1",
        'paper_five' => "Attestation de bourse au mérite",
    ],
    'generic' => [
        'download' => "Télécharger",
        'french' => "Français",
        'english' => "Anglais",
        'home' => "Acceuil",
        'pp' => "Projet Personnel",
        'up' => "Projet Universitaire",
        'tp' => "Projet Tuteuré",
        'other' => "Autre",
        'discribe' => "Description",
        'teacher' => "Encadrant",
        'claireDavid' => " CLaire David <br/>LIGM, Université Gustave Eiffel, CNRS<br/>https://orcid.org/0000-0002-3041-7013<br/>claire.david@univ-eiffel.fr",
        'olivierCure' => " Olivier Curé <br/>LIGM, Université Gustave Eiffel, CNRS<br/>https://orcid.org/0009-0000-7303-5579<br/>olivier.cure@univ-eiffel.fr",
        'victorMarsaut' => " Victor Marsaut <br/>LIGM, Université Gustave Eiffel, CNRS<br/>https://orcid.org/0000-0002-2325-6004 <br/>victor.marsault@univ-eiffel.fr",
        'nadimeFrancis' => " Nadime Francis <br/>LIGM, Université Gustave Eiffel, CNRS<br/>https://orcid.org/0009-0009-4531-7435 <br/>nadime.francis@univ-eiffel.fr",
        'olivierBouillot' => " Olivier Bouillot <br/>LIGM, Université Gustave Eiffel, CNRS<br/>https://orcid.org/0000-0001-5925-9082<br/>olivier.bouillot@univ-eiffel.fr",
    ],
];