<!DOCTYPE html>
<html lang="fr">

<head>
    <link rel="stylesheet" href="./personnal-project/style.css">
    <link rel="stylesheet" href="./tutored-project/style.css">
</head>


<div class="container-page">
    <!--<div class="container-topbar">
        <div class="topbar">
            <button onclick="window.scroll(0, getOffsetTop(document.getElementById('personnal-title_one')) - 80);">Go to here</button>
            <button onclick="window.scroll(0, getOffsetTop(document.getElementById('personnal-title_two')) - 80);">Go to here two</button>
        </div>
    </div>-->
    <div class="container-personnal">
        <h2 id="personnal-title_one" class="tutored-title">
            <?php echo($text['pp']['title_one'])?>
        </h2>
        <div class="container-grid">
            <div class="container-left">
                <div class="container-discribe">
                    <h3 style="font-size:20px">
                        <?php echo($text['generic']['discribe'])?>
                    </h3>
                    <p style="font-size:18px">
                        <?php echo($text['pp']['discribe_one'])?>
                    </p>
                </div>
            </div>
            <div class="container-rigth">
                <img/>
            </div>
        </div>
    </div>

    <div class="container-personnal">
        <h2 id="personnal-title_two" class="tutored-title">
            <?php echo($text['pp']['title_two'])?>
        </h2>
        <div class="container-grid-bloc">
            <div class="container-left">
                <div class="container-discribe">
                    <h3 style="font-size:20px">
                        <?php echo($text['generic']['discribe'])?>
                    </h3>
                    <p style="font-size:18px">
                        <?php echo($text['pp']['discribe_two'])?>
                    </p>
                </div>
            </div>
            <div class="container-rigth-bottom">
                <video id="myVideo" controls="controls">
                    <source src="./.media/exemple_snack.webm" type="video/webm" />
                </video>
            </div>   
        </div> 
    </div>
</div>

<script>
function getOffsetTop( elem ) {
    var offsetTop = 0;
    do {
        if ( !isNaN( elem.offsetTop ) ) {
            offsetTop += elem.offsetTop;
        }
    } while( elem = elem.offsetParent );
    return offsetTop;
}
//document.documentElement.scrollTop a chercher pour le défilement
</script>

</html>