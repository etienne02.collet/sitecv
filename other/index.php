<!DOCTYPE html>
<html lang="fr">

<head>
    <link rel="stylesheet" href="./other/style.css">
</head>


<body>
    <div class="container-paper">
        <h2>
            <?php echo($text['other']['title_one'])?>
        </h2>
        <div class="container-download">
            <h3 class="paper-title">
                <?php echo($text['other']['paper_one'])?>
            </h3>
            <a href="./.media/relever_note_collet_etienne_L1.pdf" download="CV-2023" class="paper-download">
                <?php echo($text['generic']['download'])?>
            </a>
        </div>
        <div class="container-download">
            <h3 class="paper-title">
                <?php echo($text['other']['paper_two'])?>
            </h3>
            <a href="./.media/relever_note_collet_etienne_L2.pdf" download="CV-2023" class="paper-download">
                <?php echo($text['generic']['download'])?>
            </a>
        </div>
        <div class="container-download">
            <h3 class="paper-title">
                <?php echo($text['other']['paper_three'])?>
            </h3>
            <a href="./.media/relever_note_collet_etienne_L3.pdf" download="CV-2023" class="paper-download">
                <?php echo($text['generic']['download'])?>
            </a>
        </div>
        <div class="container-download">
            <h3 class="paper-title">
                <?php echo($text['other']['paper_four'])?>
            </h3>
            <a href="./.media/relever_note_collet_etienne_M1.pdf" download="CV-2023" class="paper-download">
                <?php echo($text['generic']['download'])?>
            </a>
        </div>
        <div class="container-download">
            <h3 class="paper-title">
                <?php echo($text['other']['paper_five'])?>
            </h3>
            <a href="./.media/Attestation-Etienne-COLLET-2023-1.pdf" download="CV-2023" class="paper-download">
                <?php echo($text['generic']['download'])?>
            </a>
        </div>
    </div>
</body>

</html>